local color = require("color")

local function hcloud_prompt_filter()
    f = assert (io.popen ("hcloud context active"))
    for line in f:lines() do
        hcloud_context = '(hcc: ' .. line .. ')'
        break
    end -- for loop
    f:close()
    hcloud_context = color.color_text(hcloud_context, color.YELLOW)
    clink.prompt.value = clink.prompt.value:gsub("\n", hcloud_context .. "\n")
    
    return false
end

clink.prompt.register_filter(hcloud_prompt_filter, 1)