local color = require("color")

local function is_terraform()
    for dir in io.popen([[dir ".\" /b /ad]]):lines() do
        -- print(dir)
        if dir == ".terraform" then
            return true
        end
    end

    return false
end

local function hcloud_prompt_filter()
    if is_terraform() then
        f = assert (io.popen ("hcloud context active"))
        for line in f:lines() do
            hcloud_context = '(hcc: ' .. line .. ')'
            break
        end -- for loop
        f:close()
        
        f = assert (io.popen ("hcloud context active"))
        for line in f:lines() do
            if string.match(line, "production") then
                prompt_color = color.CYAN
            elseif string.match(line, "development") then
                prompt_color = color.GREEN
            else
                prompt_color = color.RED
            end
            break
        end -- for loop
        
        hcloud_context = color.color_text(hcloud_context, prompt_color)
        clink.prompt.value = clink.prompt.value:gsub("\n", hcloud_context .. "\n")

        return false
    end
end

clink.prompt.register_filter(hcloud_prompt_filter, 1)
