local color = require("color")

local function is_terraform()
    for dir in io.popen([[dir ".\" /b /ad]]):lines() do 
        -- print(dir) 
        if dir == ".terraform" then
            return true
        end 
    end
    
    return false
end

local function terraform_prompt_filter()
    if is_terraform() then
        -- print("terraform found")
        f = assert (io.popen ("terraform workspace show"))
        for line in f:lines() do
            terraform_context = '(tfws: ' .. line .. ')'
            if line == "production" then
                prompt_color = color.CYAN
            elseif line == "development" then
                prompt_color = color.GREEN
            else
                prompt_color = color.RED
            end
            break
        end -- for loop
        f:close()
        terraform_context = color.color_text(terraform_context, prompt_color)
        clink.prompt.value = clink.prompt.value:gsub("\n", terraform_context .. "\n")
        return false
    end 
end

clink.prompt.register_filter(terraform_prompt_filter, 1)